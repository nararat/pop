import mysql.connector
config = {
    "host": 'localhost',
    "user": 'root',
    "passwd": '',
    "database":'python'
}

def query(SQL_for_query_statement):
    TAG = 'DB connect : '
    my_database_connector = mysql.connector.connect(**config)
    try:
        print(my_database_connector.is_connected())
        my_database_cursor = my_database_connector.cursor()
        my_database_cursor.execute(SQL_for_query_statement)
        mykey = my_database_cursor.column_names
        result = my_database_cursor.fetchall()
        my_database_cursor.close()
        my_database_connector.close()
        # Return data values from database query at 'result' variable
        return result
    except Exception as inst:

        # If error return "database dose not connect"
        if my_database_connector.is_connected():
            my_database_connector.close()
        print(TAG, "dbcon", inst)
        return {"message": "database dose not connect"}, 500

def execute(sql):#เช็ดค่าว่าตรงกับดาต้าเบสโดยไม่ต้องบันทึกค่าลงดาต้าเบส
    my_database_connector = mysql.connector.connect(**config)
    try:
        my_database_cursor = my_database_connector.cursor()
        my_database_cursor.execute(sql)
    except Exception as er:
        if my_database_connector.is_connected():
            my_database_connector.close()
        print('execute er = ', er)
        return 'error', 500
    else:
        my_database_connector.commit()
        my_database_cursor.close()
        my_database_connector.close()
        return {
            'msg':'Success'
        }