import string
from  flask_jwt_extended import (create_access_token,JWTManager,jwt_required)
import random
import smtplib
from email.mime.text import MIMEText
from flask import Flask, request,jsonify
import  re
# from flask.ext.bcrypt import Bcrypt
from distutils.util import execute

import requests

from second_file import test_print
from database_connector import *
#import second_file
import bcrypt


my_app = Flask(__name__)
# -----------------token
my_app.config['JWT_SECRET_KEY'] = 'test'
my_app.config["JWT_ACCESS_TOKEN_EXPIRES"] = 60
my_app.config["JWT_ALGORITHM"] = "HS256"
jwt = JWTManager(my_app)


@my_app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Expose-Headers', 'X-Token')
    response.headers.add('Access-Control-Allow-Headers',
                         'Content-Type,Authorization')
    response.headers.add('Access-Control-Allow-Methods',
                         'GET,PUT,POST,DELETE,OPTIONS')
    response.headers.add('Cache-Control', 'no-cache')
    return response

@my_app.route("/login/<username>", methods = ['GET'])      #get ส.เข้าผ่านเวบบราวเซอได้ และจะเหนข้อมู,ตอนส่ง
def myFirstApi(username):
    #return second_file.test_print()
    return username

@my_app.route("/register", methods = ['POST'])      #get ส.เข้าผ่านเวบบราวเซอได้ และจะเหนข้อมู,ตอนส่ง
def Second_funtion():
    req = request.get_json()            #เปน ฟังชันที่ใช้รับ request json
    return req

@my_app.route("/users", methods=['GET'])
def users_funtion():
    sql = "SELECT * FROM user_list"
    return jsonify(query(sql))

#
# @my_app.route("/addusers", methods=['POST'])#register
# def addusers_function():
#
#     req = request.get_json()
#     username = req['name']
#     password = req['password']
#     fullname = req['fullname']
#     email = req['email']
#     address = req['address']
#     sql = "INSERT INTO user_list (name,password,fullname,email,address) " \
#           "VALUES ('%s','%s','%s','%s','%s')" % (username, password, fullname, email, address)
#
#     try:
#         values = execute(sql)
#     except Exception as er:
#         print(er)
#         return 'error', 500
#     else:
#         return jsonify(values)
#
#
#
# @my_app.route("/getLogin", methods=['POST'])#login
# def Login():
#
#     req = request.get_json()
#     username = req['name']
#     password = req['password']
#
#     sql = "SELECT * FROM user_list WHERE name = '%s' AND password = '%s' " % (username, password)
#     data = query(sql)
#     if not data:
#         return {"msg": "Fail"}, 400
#     else:
#         return {"msg": "Success "}, 200
#
#
#
# @my_app.route("/edit", methods=['POST'])
# def edit_function():
#
#     req = request.get_json()
#     username = req['name']
#     password = req['password']
#     fullname = req['fullname']
#     email = req['email']
#     address = req['address']
#     sql = "update user_list set name = '%s', password = '%s', fullname = '%s', email = '%s', address = '%s' WHERE id = 1 " % (
#     username, password, fullname, email, address)
#
#     try:
#             values = execute(sql)
#     except Exception as er:
#         print(er)
#         return 'error', 500
#     else:
#         return jsonify(values)





@my_app.route("/loginmean", methods=['POST'])
def loginmean_function(my_result=None):
    TAG = "pop"
    req = request.get_json()
    username = req['username']
    password = req['password']# password from request
    password = password.encode('utf8')
    print("password = ",password)


    sql = "SELECT password FROM user_list WHERE username = '%s' " % (username) #เอาชื่อไปเช้ดในดาต้าเบส

    data = query(sql)
    print(data)
    if not data:
        return {"msg":"fail"}
    else:
        hashed = data[0][0]  # ดึง password from database
        hashed = hashed.encode('utf8')
        print("hashed =",hashed)
        print(TAG, "password= ", password, "type= ", type(password))
        print(TAG, "hashed= ", hashed, "type= ", type(hashed))
        if bcrypt.checkpw(password, hashed):
            return {"msg": "Success","token":token_generator("pop")}, 200

        else:
            return  {"msg": "Fail"}, 400


# @my_app.route("/registerm", methods = ['POST'])      #get ส.เข้าผ่านเวบบราวเซอได้ และจะเหนข้อมู,ตอนส่ง
# def re_funtion():
#     try:
#         req = request.get_json()            #เปน ฟังชันที่ใช้รับ request json
#         username = req['name']
#         password = req['password']# password from request
#         fullname = req['fullname']
#         email = req['email']
#         address = req['address']
#     except Exception as er:
#         return {"msg": "Fail"}, 400
#     hashed = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(16))
#     hashed = hashed.decode('utf8')
#
#     pp = "INSERT INTO user_list (name,password,fullname,email,address) " \
#         "VALUES ('%s','%s','%s','%s','%s')" % (username,  hashed, fullname, email, address)
#
#     try:
#         values = execute(pp)
#     except Exception as er:
#         print(er)
#         return 'error', 500
#     else:
#         return jsonify(values)


@my_app.route("/registerm", methods = ['POST'])      #get ส.เข้าผ่านเวบบราวเซอได้ และจะเหนข้อมู,ตอนส่ง
def re_funtion():
    try:

        req = request.get_json()     #เปน ฟังชันที่ใช้รับ request json
        print(req)
        username = req['username']
        firstname = req['firstname']
        password = (str(req['password']))# password from request
        lastname = req['lastname']
        email = req['email']
        citizenid = req['citizenid'].replace("-","")
        birthdate = req['birthdate']

        sql = "SELECT * FROM  user_list WHERE  username = '%s' " % (username)  # เอาชื่อไปเช้ดในดาต้าเบส
        account = query(sql)
        print("account = ",account)

        if account:
            return ('Account already exists!')
        else:
            if not firstname or not lastname or not email :
                return ('Please fill out the form!')
            elif not re.fullmatch(r'[A-Za-z0-9]+', username):
                return ('Username must contain only characters and numbers!')
            elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
                return ('Invalid email address!')
            else:

                print(type(password))
                hashed = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(16))
                hashed = hashed.decode('utf8')

                sql = "INSERT INTO user_list (username, firstname,password,lastname,email,citizenid,birthdate) " \
                      "VALUES ('%s','%s','%s','%s','%s','%s','%s')" % (username,firstname, hashed, lastname, email, citizenid,birthdate)

                try:
                    values = execute(sql)
                    print(values)
                except Exception as er:
                    print(er)
                    return 'error', 500
                else:
                    return jsonify(values)

    except Exception as er:
        print(er)
        return {"msg": "Fail"}, 500



# @my_app.route("/loginmean", methods=['POST'])
# def loginmean_function(my_result=None):
#     TAG = "pop"
#     req = request.get_json()
#     username = req['username ']
#     password = req['password']# password from request
#     password = password.encode('utf8')
#     # password = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(16))
#     print("password = ",password)
#
#
#     sql = "SELECT password FROM user_list WHERE username  = '%s' " % (username) #เอาชื่อไปเช้ดในดาต้าเบส
#
#     data = query(sql)
#     print(data)
#     if not data:
#         return {"msg":"fail"}
#     else:
#         hashed = data[0][0]  # ดึง password from database
#         hashed = hashed.encode('utf8')#เปนการเข้ารหัส
#         print("hashed =",hashed)
#         print(TAG, "password= ", password, "type= ", type(password))
#         print(TAG, "hashed= ", hashed, "type= ", type(hashed))
#         if bcrypt.checkpw(password, hashed):
#             return {"msg": "Success"}, 200
#
#         else:
#             return  {"msg": "Fail"}, 400


@my_app.route("/forgetpassword", methods = ['PUT'])      #get ส.เข้าผ่านเวบบราวเซอได้ และจะเหนข้อมู,ตอนส่ง
def forget_funtion():
    try:

        req = request.get_json()     #เปน ฟังชันที่ใช้รับ request json
        print(req)
        username = req['username']
        email = req['email']
        citizenid = req['citizenid'].replace("-","")
        birthdate = req['birthdate']

        sql = "SELECT * FROM  user_list WHERE   username = '%s' AND citizenid = '%s'AND email = '%s'" % (username, citizenid,email)  # เอาชื่อไปเช้ดในดาต้าเบส
        print(sql)

        account = query(sql)
        if not account:
            return ('pop not found')
        print("account = ",account)

        if account:
            password = randomString(10)
            hashed = bcrypt.hashpw(password.encode('utf8'), bcrypt.gensalt(16))
            hashed = hashed.decode('utf8')
            print(password)
            sql = "update user_list set password = '%s' WHERE username = '%s' " % (hashed,username)
            execute(sql)

            ms= 'new password = %s'%password
            send_email(email, 'new password', ms)
            print(ms)
            return {"msg": "success"},200

    except Exception as er:
        print(er)
        return {"msg": "error"}, 500





sender_email = '5710110222@psu.ac.th'
sender_password = 'aa08082538.'

def send_email(mailto, subject, msg):
    try:
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()

        server.login(sender_email, sender_password)
        message = 'Subject: {}\n\n{}'.format(subject, msg)
        server.sendmail(sender_email, mailto, message)
        server.quit()
    except Exception as er:
        print('er = ', er)
        pass
    else:
        print({
            "message": "Success"
        })



def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))




@my_app.route("/token", methods = ['GET'])
@jwt_required #token
def koten_funtion():
    return ('pop')



def token_generator(username):
    try:
        access_token = create_access_token(identity=username)
        return  access_token
    except Exception as er:
        print('token genarate error',er)
        return  'error',400



@my_app.route("/send", methods=['POST'])
def send_function(my_result=None):
   
    req = request.get_json()
    token = 'Bearer Ade04095c008d5a81a9bd7084e58b57caf00e5152bddd4256b7e2c8b8a0d82cd1d2c55e2df7b744ef99301ac2cca47cc5'
    message = req['message']
    payload = {
        "to": "809373383168",
        "bot_id": "Bf8f1ce2d524c503ab11a937e400a64e1",
        "type": "text",
        "message": message,
        "custom_notification": "เปิดอ่านข้อความใหม่จากทางเรา"
    }
    requests.post(url="https://chat-public.one.th:8034/api/v1/push_message", headers={'Authorization': token}, json=payload).json()
    return 'success'
















my_app.run(host="0.0.0.0",debug=True,port=9000)






